package vip.yazilim.springboot.basics.dependencyinjection.sort;

/**
 * 
 * @author Anıl Korkmaz
 *
 */
public interface SortAlgorithm {

    public int[] sort(int[] numbers);
}
