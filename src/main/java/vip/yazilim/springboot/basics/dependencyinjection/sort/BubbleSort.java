package vip.yazilim.springboot.basics.dependencyinjection.sort;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Anıl Korkmaz
 *
 */
@Component
@Primary
public class BubbleSort implements SortAlgorithm {

    @Override
    public int[] sort(int[] numbers) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
	public String toString() {
		return getClass().getSimpleName();
	}
}
