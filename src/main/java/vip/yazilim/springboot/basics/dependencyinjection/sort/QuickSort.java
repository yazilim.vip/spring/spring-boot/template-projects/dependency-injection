package vip.yazilim.springboot.basics.dependencyinjection.sort;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Anıl Korkmaz
 *
 */
@Component
@Qualifier(value = "firstWithQualifier")
public class QuickSort implements SortAlgorithm {

	@Override
	public int[] sort(int[] numbers) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
}
