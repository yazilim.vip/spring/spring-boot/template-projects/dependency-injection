package vip.yazilim.springboot.basics.dependencyinjection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class BasicsApplication {

	public static void main(String[] args) {
		ApplicationContext appctx = SpringApplication.run(BasicsApplication.class, args);

		BinarySearchImpl bsi =  appctx.getBean(BinarySearchImpl.class);
		System.err.println(bsi.getSortAlgorithm());
		System.err.println(bsi.getSortAlgorithm2());

	}

}
