package vip.yazilim.springboot.basics.dependencyinjection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import vip.yazilim.springboot.basics.dependencyinjection.sort.SortAlgorithm;

/**
 * 
 * @author Anıl Korkmaz
 *
 */
@Component
public class BinarySearchImpl {

	@Autowired
	private SortAlgorithm sortAlgorithm;

	@Autowired
	@Qualifier("firstWithQualifier")
	private SortAlgorithm sortAlgorithm2;

	public SortAlgorithm getSortAlgorithm() {
		return sortAlgorithm;
	}

	public SortAlgorithm getSortAlgorithm2() {
		return sortAlgorithm2;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "@" + hashCode();
	}

}
